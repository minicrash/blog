const commentaireModel = require('../schema/commentaire')
module.exports = {
    listerCommentaire : () => {

        return new Promise(function(resolve, reject){
            commentaireModel.find((err, commentaire) => {
               if(err) reject(err)
               resolve(commentaire)
           })
        })
    },
    ajouterCommentaire : (pseudo, contenu) =>{
        return new Promise(function(resolve, reject){
            if(pseudo === undefined && contenu === undefined) resolve()
            else{
                let newCommentaire = new commentaireModel({pseudo: pseudo, date: new Date(), contenu: contenu})
                newCommentaire.save(err => {
                    if(err) reject(err)
                    resolve()
                })
            }
        })
    },
    supprimerCommentaire : (id) =>{
        return new Promise(function(resolve, reject){
            commentaireModel.deleteOne({_id:id},err => {
                if(err) reject(err)
                resolve()
            })
        })
    } 
}