const express = require('express')
const app = express()

let connecter = require('./config/db')
let commentaireCtrl = require('./controllers/commentaire')
 
app.get('/', (req, res) => {
    res.setHeader("Content-Type","text/html") 
    let query = req.query
    connecter()
        .then(db => { 
            commentaireCtrl.ajouterCommentaire(query.pseudo, query.commentaire)
            .then(() => {
                return commentaireCtrl.listerCommentaire()
            })
            .then(commentaires => {
                res.render('index.pug', {commentaires: commentaires})
                db.close()
            })
            .catch(err => {
                console.log(err)
            })
        })
        .catch(err => {
            console.log("Db connection failed : ", err)
        })
    
})
.get('/commenter', (req,res) => {
    res.setHeader("Content-Type","text/html") 
    res.render('commenter.pug')
})
.get('/:id/supprimer', (req,res) =>{
    res.setHeader("Content-Type","text/html")
    if(req.params.id){
        connecter()
        .then(db => {
            commentaireCtrl.supprimerCommentaire(req.params.id)
            .then(()=>{
                return commentaireCtrl.listerCommentaire()
            })
            .then(commentaires => {
                res.render('index.pug', {commentaires: commentaires})
                db.close()
            })
            .catch(err => {
                console.log(err)
                db.close()
            })
        })
        .catch(err => {
            console.log("Db connection failed : ", err)
        })
    }
})

app.listen(3000, () => {
    console.info("Listenning on 3000 port")
})