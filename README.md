# Blog 
Un blog basique en nodeJS mongo dans une instance docker

## Getting started
Si docker d'installé : 
```
$ docker-compose up
```

Sinon avoir la LTS de node et mongo d'installer. Créer une collection blog
```
$ npm install && npm start
```