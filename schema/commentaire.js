const mongoose = require('mongoose')

let commentaireSchema = mongoose.Schema({
    pseudo : { type : String, match: /^[a-zA-Z0-9-_]+$/ },
    contenu : {type : String, required: true},
    date : { type : Date, default : Date.now }
})
commentaireSchema.statics.initCommentaire = (Commantaire) => {
    let commantaire = {
        pseudo: "Titi",
        contenu: "Welcome",
        date: new Date()
    }

    Commantaire.remove({}, (err) => {
        Commantaire.create(commantaire)
    })
}
let commentaireModel = mongoose.model('commentaires', commentaireSchema)
module.exports = commentaireModel