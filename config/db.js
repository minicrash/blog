const mongoose = require('mongoose')
const url = "mongodb://mongo:27017/blog"
const Commentaire = require("../schema/commentaire")
let connect = () => {
    return new Promise((resolve,reject) => {
        mongoose.connect(url)
        let db = mongoose.connection 
        db.on('error', function(){
            console.error('connexion db failed')
            return reject('error connection db')
        })
        .once('open', function () {
            Commentaire.initCommentaire(Commentaire)
            console.info('connection etablished')
            return resolve(db)
        })
    })
}
module.exports = connect